<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginBase;

/**
 * Generator process plugin for "Price" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "commerce_price"
 * )
 */
class CommercePriceExport extends GeneratorExportPluginBase {
}

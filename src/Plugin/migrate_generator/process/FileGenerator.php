<?php

namespace Drupal\migrate_generator\Plugin\migrate_generator\process;

use Drupal\migrate_generator\Plugin\GeneratorProcessPluginBase;

/**
 * Generator process plugin for "File" field type.
 *
 * @GeneratorProcessPlugin(
 *   id = "file"
 * )
 */
class FileGenerator extends GeneratorProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function process($field_name) {
    $process[$field_name] = $this->getBaseProcess($field_name);

    $process[$field_name][] = [
      'plugin' => 'to_array',
    ];

    if ($this->getFieldStorageDefinition()->getCardinality() == 1) {
      $process[$field_name][] = [
        'plugin' => 'to_array',
        'force' => TRUE,
      ];
    }

    $process[$field_name][] = $this->getSubProcess($field_name);

    return $process;
  }

  /**
   * Get subprocess configuration to copy file.
   *
   * @param string $field_name
   *   Field name.
   *
   * @return array
   *   Subprocess configuration.
   */
  protected function getSubProcess($field_name) {
    $subprocess = [
      'plugin' => 'sub_process',
      'process' => [
        'source' => '0',
        'tmp_basename' => [
          'plugin' => 'callback',
          'callable' => 'basename',
          'source' => '0',
        ],
        'tmp_param' => [
          'plugin' => 'default_value',
          'default_value' => PHP_URL_PATH,
        ],
        'filename' => [
          'plugin' => 'callback',
          'callable' => 'parse_url',
          'unpack_source' => TRUE,
          'source' => [
            '@tmp_basename',
            '@tmp_param',
          ],
        ],
        'target_directory' => [
          'plugin' => 'default_value',
          'default_value' => $this->getTargetDirectory($field_name),
        ],
        'destination' => [
          'plugin' => 'concat',
          'source' => [
            '@target_directory',
            '@filename',
          ],
        ],
        'final_file' => [
          [
            'plugin' => 'skip_on_404',
            'method' => 'process',
            'source' => '@source',
          ],
          [
            'plugin' => 'file_copy',
            'source' => [
              '@source',
              '@destination',
            ],
            'file_exists' => 'rename',
          ],
        ],
        'target_id' => [
          'plugin' => 'entity_generate',
          'entity_type' => 'file',
          'value_key' => 'uri',
          'source' => '@final_file',
        ],
      ],
    ];
    // Add support for description property.
    $sources = $this->getSources();
    if (isset($sources['description'])) {
      $subprocess['include_source'] = TRUE;
      $subprocess['source_key'] = 'root';
      $subprocess['process']['description'] = 'root/' . $sources['description'];
    }
    // Transform relative filepaths in CSV to absolute.
    if ($this->options['relative_filepath'] && $this->options['source_folder']) {
      $subprocess['process'] = [
        'source_directory' => [
          'plugin' => 'default_value',
          'default_value' => $this->options['source_folder'] . DIRECTORY_SEPARATOR,
        ],
      ] + $subprocess['process'];
      $subprocess['process']['source'] = [
        'plugin' => 'concat',
        'source' => [
          '@source_directory',
          '0',
        ],
      ];
    }
    return $subprocess;
  }

  /**
   * Get directory name for migrating file.
   *
   * @param string $field_name
   *   Field name.
   *
   * @return string
   *   URI of destination folder.
   */
  private function getTargetDirectory($field_name) {
    return $this->getFieldStorageDefinition()->getSetting('uri_scheme') . '://' . $field_name . DIRECTORY_SEPARATOR;
  }

}

<?php

namespace Drupal\migrate_generator\Plugin\migrate_generator\process;

use Drupal\migrate_generator\Plugin\GeneratorProcessPluginDateBase;

/**
 * Generator process plugin for "Date recur" field type.
 *
 * @GeneratorProcessPlugin(
 *   id = "date_recur"
 * )
 */
class DateRecurGenerator extends GeneratorProcessPluginDateBase {
}

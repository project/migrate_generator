<?php

namespace Drupal\migrate_generator_export\Controller;

use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\State\StateInterface;
use Drupal\system\FileDownloadController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Returns file to download.
 */
class ExportController implements ContainerInjectionInterface {

  /**
   * The file download controller.
   *
   * @var \Drupal\system\FileDownloadController
   */
  protected $fileDownloadController;

  /**
   * The CSRF token generator.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfToken;

  /**
   * State service for retrieving database info.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      FileDownloadController::create($container),
      $container->get('csrf_token'),
      $container->get('state')
    );
  }

  /**
   * Constructs a ExportController object.
   *
   * @param \Drupal\system\FileDownloadController $file_download_controller
   *   The file download controller.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrf_token
   *   The CSRF token generator.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(FileDownloadController $file_download_controller, CsrfTokenGenerator $csrf_token, StateInterface $state) {
    $this->fileDownloadController = $file_download_controller;
    $this->csrfToken = $csrf_token;
    $this->state = $state;
  }

  /**
   * Downloads a tarball of the selected content.
   *
   * @param string $uri
   *   The URI to download.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The downloaded file.
   */
  public function downloadExport($uri, Request $request) {
    if ($uri) {
      // If user comes from drush, then use state storage for token validation.
      if (!empty($request->query->get('drush'))) {
        $value = $this->state->get('migrate_generator_export.token');
        $this->state->delete('migrate_generator_export.token');
        if (!hash_equals($value, $request->query->get('token'))) {
          throw new AccessDeniedHttpException();
        }
      }
      // @todo Simplify once https://www.drupal.org/node/2630920 is solved.
      elseif (!$this->csrfToken->validate($request->query->get('token'), $uri)) {
        throw new AccessDeniedHttpException();
      }

      $request = new Request(['file' => $uri]);
      return $this->fileDownloadController->download($request, 'temporary');
    }
  }

}

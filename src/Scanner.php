<?php

namespace Drupal\migrate_generator;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use League\Csv\Reader;
use Psr\Log\LoggerInterface;

/**
 * Class for migrate source scanner.
 */
class Scanner {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Source scanner constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityFieldManager $entity_field_manager
   *   The entity field manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityFieldManager $entity_field_manager,
    LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->logger = $logger;
  }

  /**
   * Read directory with source CSV files.
   *
   * @param string $directory
   *   Path to directory with source .csv files.
   * @param array $options
   *   Additional options.
   * @param bool $is_translation
   *   TRUE for checking translations.
   *
   * @return array
   *   Array of source infos.
   */
  public function readSources(string $directory, array $options, bool $is_translation = FALSE) {
    $sources = [];
    if ($is_translation) {
      // Check in /translation subfolder.
      $directory = $directory . DIRECTORY_SEPARATOR . 'translation';
    }
    // Read files from specified folder.
    if (!is_dir($directory) && !$is_translation) {
      throw new \Exception('The destination directory does not exist.');
    }
    // Look for source files in defined directory.
    $source_files = glob($directory . DIRECTORY_SEPARATOR . $options['pattern']);
    if (empty($source_files) && !$is_translation) {
      throw new \Exception('Source files not found in the destination directory.');
    }

    foreach ($source_files as $source_file) {
      // Extract entity type, bundle from the file name. Supported:
      // {entity_type}-{bundle}.csv or {entity_type}.csv.
      preg_match('!
        ([a-z_]+)             # entity type (group 1)
        (
          \\-                 # -
          ([^\\./]+)          # bundle (group 3)
        )?                    # (can be empty)
        \\.                   # .
        csv                   # csv extension
      $!x', $source_file, $matches);
      if (isset($matches[1])) {
        $entity_type = $matches[1];
        $bundle = $matches[3] ?? $entity_type;
      }
      else {
        // Skip if cannot parse source file name.
        $this->logger->alert(
          'Cannot parse source filename %name. Skipping.',
          ['%name' => $source_file]
        );
        continue;
      }

      $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type);
      // Skip if bundle not exists.
      if (!($bundle_info && isset($bundle_info[$bundle]))) {
        $this->logger->warning(
          'Bundle %bundle does not exist for %entity entity. Skipping.',
          [
            '%bundle' => $bundle,
            '%entity' => $entity_type,
          ]
        );
        continue;
      }

      // Read source file headers.
      $reader = Reader::createFromPath($source_file, 'r');
      $reader->setDelimiter($options['delimiter']);
      $reader->setEnclosure($options['enclosure']);
      $reader->setHeaderOffset(0);
      $header = $reader->getHeader();

      $source_data = [
        'migration_id' => $this->getMigrationId($entity_type, $bundle, $options['tag']),
        'source' => $source_file,
        'header' => $header,
        'id' => reset($header),
        'id_key' => $this->entityTypeManager->getDefinition($entity_type)->getKey('id'),
        'revision_key' => $this->entityTypeManager->getDefinition($entity_type)->getKey('revision'),
      ];
      $sources[$entity_type][$bundle] = $source_data;
    }
    // Once all sources were scanned,
    // we can check field definition and dependencies.
    $this->checkFields($sources);
    return $sources;
  }

  /**
   * Update source array with fields definition and dependencies.
   *
   * @param array $sources
   *   Source info array.
   */
  protected function checkFields(array &$sources) {
    foreach ($sources as $entity_type => &$entity_info) {
      foreach ($entity_info as $bundle => &$source_info) {
        $fields_info = [];
        $instances = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
        foreach ($source_info['header'] as &$column) {
          // Do not use first column
          // It will be used only for internal ID mapping.
          if ($column == $source_info['id']) {
            continue;
          }
          // Check if this field has several properties.
          $item = explode('/', $column);
          $fieldname = $item[0];
          $fieldkey = $item[1] ?? NULL;
          if (isset($fields_info[$fieldname])) {
            $field_info = $fields_info[$fieldname];
          }
          else {
            $field_info = [
              'fieldname' => $fieldname,
              'multiple' => FALSE,
              'is_reference' => FALSE,
            ];
          }
          if ($fieldkey) {
            // Replace '/' with '__' in column name.
            $column = str_replace('/', '__', $column);
            $field_info['source'][$fieldkey] = $column;
          }
          else {
            $field_info['source'][] = $column;
          }

          // Check if we have such field in this entity.
          if (isset($instances[$fieldname])) {
            $field_info['field_type'] = $instances[$fieldname]->getType();
            $field_info['field_storage_definition'] = $instances[$fieldname]->getFieldStorageDefinition();
            $reference_target_type = $target_bundles = $negate = NULL;
            // Get target info for field types.
            switch ($field_info['field_type']) {
              case 'image':
              case 'file':
                $reference_target_type = 'file';
                $target_bundles = ['file'];
                break;

              case 'entity_reference':
              case 'entity_reference_revisions':
                $reference_definition = $instances[$fieldname]->getItemDefinition()->getSettings();
                $reference_target_type = $reference_definition['target_type'];
                if (isset($reference_definition['handler_settings']['negate'])) {
                  $negate = $reference_definition['handler_settings']['negate'];
                }
                if (!empty($reference_definition['handler_settings']['target_bundles'])) {
                  $target_bundles = $reference_definition['handler_settings']['target_bundles'];
                }
                elseif ($reference_definition['handler'] == 'views') {
                  $view_name = $reference_definition['handler_settings']['view']['view_name'];
                  $view_display = $reference_definition['handler_settings']['view']['display_name'];
                  $target_definition = $this->entityTypeManager->getDefinition($reference_target_type);
                  // Get bundle from view filter if possible.
                  $view = $this->entityTypeManager->getStorage('view')->load($view_name);
                  if ($view) {
                    $bundle_field = $target_definition->getKey('bundle');
                    $display = $view->getDisplay($view_display);
                    if ($bundle_field && isset($display['display_options']['filters'])) {
                      foreach ($display['display_options']['filters'] as $filter) {
                        if ($filter['entity_type'] == $reference_target_type && $filter['entity_field'] == $bundle_field) {
                          $target_bundles = $filter['value'];
                        }
                      }
                    }
                  }
                }

                if (empty($target_bundles)) {
                  // Get all bundles for referenced target type.
                  $target_bundles = array_keys($this->entityTypeBundleInfo->getBundleInfo($reference_target_type));
                  // Exclude current bundle.
                  if ($reference_target_type == $entity_type) {
                    $target_bundles = array_diff($target_bundles, [$bundle]);
                  }
                }
                break;
            }
            if ($reference_target_type && $target_bundles) {
              $field_info['is_reference'] = TRUE;
              $field_info['reference_target_type'] = $reference_target_type;
              $field_info['target_bundles'] = $target_bundles;
              $field_info['negate'] = $negate;
              // Check if we have sources for referenced entity type.
              if (isset($sources[$reference_target_type])) {
                $candidacies = array_intersect(array_keys($sources[$reference_target_type]), $target_bundles);
                if ($negate) {
                  $candidacies = array_diff(array_keys($sources[$reference_target_type]), $target_bundles);
                }
                if ($candidacies) {
                  foreach ($candidacies as $candidacy) {
                    // If have source then add dependency.
                    if (isset($sources[$reference_target_type][$candidacy])
                      && !empty($sources[$reference_target_type][$candidacy]['migration_id'])) {
                      $field_info['dependencies'][] = $sources[$reference_target_type][$candidacy]['migration_id'];
                    }
                  }
                }
              }
            }

            if ($instances[$fieldname]->getFieldStorageDefinition()->getCardinality() != 1) {
              $field_info['multiple'] = TRUE;
            }

            $fields_info[$fieldname] = $field_info;
          }
          else {
            $this->logger->warning(
              'Field %field not found for %bundle %entity.',
              [
                '%field' => $fieldname,
                '%bundle' => $bundle,
                '%entity' => $entity_type,
              ]
            );
          }
        }
        $source_info['fields'] = $fields_info;
      }
    }
  }

  /**
   * Generate migration Id.
   *
   * @param string $entity_type
   *   Destination entity type.
   * @param string $bundle
   *   Destination bundle.
   * @param string $tag
   *   Migration tag.
   *
   * @return string
   *   Migration Id.
   */
  protected function getMigrationId(string $entity_type, string $bundle, string $tag) {
    if ($entity_type == $bundle) {
      return $tag . '__' . $entity_type;
    }
    return $tag . '__' . $entity_type . '__' . $bundle;
  }

}

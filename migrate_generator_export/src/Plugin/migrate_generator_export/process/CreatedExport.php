<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginDateBase;

/**
 * Generator process plugin for "Created" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "created"
 * )
 */
class CreatedExport extends GeneratorExportPluginDateBase {
}

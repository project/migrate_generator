<?php

namespace Drupal\migrate_generator\Plugin\migrate_generator\process;

/**
 * Generator process plugin for "Image" field type.
 *
 * @GeneratorProcessPlugin(
 *   id = "image"
 * )
 */
class ImageGenerator extends FileGenerator {

  /**
   * {@inheritdoc}
   */
  protected function getSubProcess($field_name) {
    $subprocess = parent::getSubProcess($field_name);
    $sources = $this->getSources();
    // Add support for alt property.
    if (isset($sources['alt'])) {
      $subprocess['include_source'] = TRUE;
      $subprocess['source_key'] = 'root';
      $subprocess['process']['alt'] = 'root/' . $sources['alt'];
    }
    // Add support for title property.
    if (isset($sources['title'])) {
      $subprocess['include_source'] = TRUE;
      $subprocess['source_key'] = 'root';
      $subprocess['process']['title'] = 'root/' . $sources['title'];
    }
    return $subprocess;
  }

}

<?php

namespace Drupal\migrate_generator_export;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityInterface;
use Psr\Log\LoggerInterface;

/**
 * Class FieldsExportManager for exporting fields values.
 */
class FieldsExportManager {

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The generator process plugin manager.
   *
   * @var \Drupal\migrate_generator\GeneratorProcessPluginManager
   */
  protected $exportPluginManager;

  /**
   * Constructs a FieldsExportManager object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\migrate_generator_export\GeneratorExportPluginManager $export_plugin_manager
   *   The generator export plugin manager.
   */
  public function __construct(LoggerInterface $logger, GeneratorExportPluginManager $export_plugin_manager) {
    $this->logger = $logger;
    $this->exportPluginManager = $export_plugin_manager;
  }

  /**
   * Gets values for the given entity fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to export.
   * @param array $fields
   *   Array of fields to pass to the operation callback.
   * @param array $options
   *   Array of export options.
   *
   * @return array
   *   Array with fields values.
   */
  public function getFieldsValues(ContentEntityInterface $entity, array $fields, array $options) {
    $values = [];
    $files = [];
    foreach ($fields as $field) {
      $field_sub_value_name = '';
      if (strpos($field, '-') !== FALSE) {
        [$field_name, $field_sub_value_name] = explode('-', $field);
      }
      if (!empty($field_sub_value_name)) {
        $field = $field_name;
      }
      if ($field != 'path' && $entity->get($field)->isEmpty()) {
        $values[] = '';
        continue;
      }
      $field_type = $entity->get($field)->getFieldDefinition()->getType();
      try {
        $plugin = $this->exportPluginManager->createInstance($field_type, $options);
        $value = $plugin->process($entity, $field, $field_sub_value_name);
        if (is_array($value) && !empty($value['output']) && !empty($value['files'])) {
          $files = array_merge($files, $value['files']);
          $value = $value['output'];
        }
        $values[] = $value;
      }
      catch (PluginNotFoundException $e) {
        // Save log that plugin not found for this field type.
        $this->logger->alert(
          'GeneratorExportPlugin not found for %field_type field type.', ['%field_type' => $field_type]
        );
      }
    }

    if (!empty($files)) {
      $values = [
        'files' => $files,
        'values' => $values,
      ];
    }

    return $values;
  }

}

<?php

namespace Drupal\migrate_generator_export;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Archiver\ArchiveTar;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Link;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Class ExportManager for performing export.
 */
class ExportManager {

  use StringTranslationTrait;

  /**
   * Entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfoService;

  /**
   * Constructs an ExportManager object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   Entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service
   *   The bundle info service.
   */
  public function __construct(EntityFieldManagerInterface $field_manager, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info_service) {
    $this->fieldManager = $field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfoService = $bundle_info_service;
  }

  /**
   * Check referenced dependencies to other config exports.
   *
   * @param string $export_id
   *   Config export ID.
   * @param array $entity_types
   *   Entity types definitions array.
   * @param array $dependencies
   *   Array of export config.
   * @param array $errors
   *   Array with potential errors.
   */
  public function checkDependencies($export_id, array $entity_types, array &$dependencies, array &$errors) {
    $to_be_added = [];
    $export = $this->entityTypeManager->getStorage('entity_type_export_config')->load($export_id);
    [$entity_type_id, $bundle] = explode('--', $export_id);
    if (empty($export)) {
      $errors[$bundle] = $this->t(
        'In order to export referenced content, please, create corresponding configuration for @type of type @bundle @here.',
        [
          '@type' => $entity_type_id,
          '@bundle' => $bundle,
          '@here' => Link::createFromRoute($this->t('here'), 'entity.entity_type_export_config.add_form',
            [
              'entity_type_id' => $entity_type_id,
              'bundle' => $bundle,
            ])->toString(),
        ]
      );
      return;
    }
    $field_types = $this->fieldManager->getFieldDefinitions($entity_type_id, $bundle);
    foreach ($export->getFields() as $field_name) {
      if (strpos($field_name, '-')) {
        $field_data = explode('-', $field_name);
        $field_name = reset($field_data);
      }
      if (in_array($field_types[$field_name]->getType(), [
        'entity_reference', 'entity_reference_revisions',
      ])) {
        $settings = $field_types[$field_name]->getSettings();
        $target_entity_type_id = $settings['target_type'];
        if ($entity_types[$target_entity_type_id]->getGroup() != 'content') {
          continue;
        }
        // Get referenced bundles for target type.
        $bundles = $referenced_bundles = array_keys($this->bundleInfoService->getBundleInfo($target_entity_type_id));
        if ($target_entity_type_id && !empty($settings['handler_settings']['target_bundles'])) {
          $referenced_bundles = array_intersect($bundles, $settings['handler_settings']['target_bundles']);
          if (!empty($settings['handler_settings']['negate'])) {
            $referenced_bundles = array_diff($bundles, $settings['handler_settings']['target_bundles']);
          }
        }
        foreach ($referenced_bundles as $referenced_bundle) {
          $id = $target_entity_type_id . '--' . $referenced_bundle;
          $export_entity = $this->entityTypeManager->getStorage('entity_type_export_config')->load($id);
          if (empty($export_entity)) {
            $errors[$referenced_bundle] = $this->t(
              'In order to export referenced content, please, create corresponding configuration for @type of type @bundle @here.',
              [
                '@type' => $target_entity_type_id,
                '@bundle' => $referenced_bundle,
                '@here' => Link::createFromRoute($this->t('here'), 'entity.entity_type_export_config.add_form',
                  [
                    'entity_type_id' => $target_entity_type_id,
                    'bundle' => $referenced_bundle,
                  ])->toString(),
              ]
            );
          }
          else {
            $to_be_added[] = $id;
          }
        }
      }
    }
    $dependencies[$export_id] = $export_id;
    if ($to_be_added) {
      foreach ($to_be_added as $key) {
        if (!in_array($key, $dependencies)) {
          // Check dependencies for this dependent export config.
          $this->checkDependencies($key, $entity_types, $dependencies, $errors);
        }
      }
    }
  }

  /**
   * Generates operations for the given entity type and its bundle.
   *
   * @param array $operations
   *   Array of operations.
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle_id
   *   Bundle ID.
   * @param array $fields
   *   Array of fields to pass to the operation callback.
   * @param array $export_settings
   *   Export settings.
   * @param int $entity_id
   *   Entity ID, if available.
   */
  public function getOperations(array &$operations, $entity_type_id, $bundle_id, array $fields, array $export_settings, $entity_id = NULL) {
    $items = [];
    if ($entity_id > 0) {
      $items[] = $entity_id;
    }
    else {
      $entity_keys = $this->entityTypeManager->getDefinition($entity_type_id)->getKeys();
      $entity_key_id = !empty($entity_keys['id']) ? $entity_keys['id'] : 'id';
      if ($entity_type_id == $bundle_id) {
        $items = $this->entityTypeManager->getStorage($entity_type_id)->getQuery()
          ->accessCheck()
          ->sort($entity_key_id)
          ->execute();
      }
      else {
        $bundle = !empty($entity_keys['bundle']) ? $entity_keys['bundle'] : 'type';
        $items = $this->entityTypeManager->getStorage($entity_type_id)->getQuery()
          ->accessCheck()
          ->condition($bundle, $bundle_id)
          ->sort($entity_key_id)
          ->execute();
      }
    }
    if (empty($items)) {
      return;
    }
    // Prepare export file.
    $file_name = $entity_type_id == $bundle_id ? $entity_type_id . '.csv' : $entity_type_id . '-' . $bundle_id . '.csv';
    $full_file_name = !empty($export_settings['folder']) ? $export_settings['folder'] . '/' . $file_name : \Drupal::service('file_system')->getTempDirectory() . '/' . $file_name;
    $handler = fopen($full_file_name, 'w');
    $header = [];
    foreach ($fields as $i => $field) {
      if ($i == 0 && strpos($field, 'id', -2) !== FALSE) {
        $field = 'id';
      }
      $header[] = str_replace('-', '/', $field);
    }
    fputcsv($handler, $header, $export_settings['delimiter'], $export_settings['enclosure']);
    fclose($handler);

    foreach ($items as $id) {
      if ($id > 0) {
        $operations[] = [
          'Drupal\migrate_generator_export\ExportManager::batchProcess',
          [
            $entity_type_id,
            $file_name,
            $id,
            $fields,
            $export_settings,
          ],
        ];
      }
    }
  }

  /**
   * Batch processing callback.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $file_name
   *   Export file name.
   * @param int $id
   *   Entity ID.
   * @param array $fields
   *   Array of fields to be exported.
   * @param array $options
   *   Array of options to be used.
   * @param array|object $context
   *   Array with context information.
   */
  public static function batchProcess($entity_type_id, $file_name, $id, array $fields, array $options, &$context) {
    if (!isset($context['results']['drush'])) {
      $context['results']['drush'] = $options['drush'];
    }
    $context['message'] = t('Processing @entity with ID = @id', [
      '@entity' => $entity_type_id,
      '@id' => $id,
    ]);
    if (!empty($options['folder'])) {
      if (!isset($context['results']['folder'])) {
        $context['results']['folder'] = $options['folder'];
      }
      $full_file_name = $options['folder'] . '/' . $file_name;
    }
    else {
      $full_file_name = \Drupal::service('file_system')->getTempDirectory() . '/' . $file_name;
    }
    $handler = fopen($full_file_name, 'a');
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($id);
    if (!empty($entity)) {
      $values = \Drupal::service('migrate_generator_export.fields_export_manager')->getFieldsValues($entity, $fields, $options);
      if (!empty($values['files'])) {
        if (empty($context['results']['file_export'])) {
          $context['results']['file_export'] = [];
        }
        $context['results']['file_export'] = array_merge($context['results']['file_export'], $values['files']);
        $values = $values['values'];
      }
      fputcsv($handler, $values, $options['delimiter'], $options['enclosure']);
    }
    fclose($handler);
    if (empty($context['results']['files'][$file_name])) {
      $context['results']['files'][$file_name]['tmp_name'] = $full_file_name;
      $context['results']['files'][$file_name]['count'] = 0;
    }
    $context['results']['files'][$file_name]['count'] = $context['results']['files'][$file_name]['count'] + 1;
  }

  /**
   * Batch finished callback.
   */
  public static function batchFinished($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      /** @var \Drupal\Core\File\FileSystemInterface $file_system */
      $file_system = \Drupal::service('file_system');
      if (!empty($results['files'])) {
        if (!empty($results['folder'])) {
          if (!empty($results['file_export'])) {
            $files_folder = $results['folder'] . '/files';
            $file_system->prepareDirectory($files_folder, FileSystemInterface::CREATE_DIRECTORY);
            foreach ($results['file_export'] as $destination_path => $source_path) {
              try {
                $destination = $files_folder . '/' . pathinfo($destination_path, PATHINFO_DIRNAME);
                $file_system->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY);
                $file_system->copy($source_path, $destination, FileSystemInterface::EXISTS_REPLACE);
              }
              catch (\Exception $e) {
                $messenger->addError(t('Error adding file "@filepath" to folder', ['@filepath' => $destination]));
              }
            }
          }
          $messenger->addMessage(t('Files can be found in pointed folder.'));
          return;
        }
        $archive_name = 'migrate-export.tar.gz';
        $tmp_archive_name = $file_system->getTempDirectory() . '/' . $archive_name;
        if (file_exists($tmp_archive_name)) {
          $file_system->delete($tmp_archive_name);
        }
        $archiver = new ArchiveTar($tmp_archive_name, 'gz');
        foreach ($results['files'] as $file_name => $tmp_file) {
          $archiver->addString($file_name, file_get_contents($tmp_file['tmp_name']));
          $file_system->delete($tmp_file['tmp_name']);
          $messenger->addMessage(\Drupal::translation()->formatPlural(
            $tmp_file['count'],
            'Exported 1 item in @filename.',
            'Exported @count items in @filename.',
            ['@filename' => $file_name]
          ));
        }
        if (!empty($results['file_export'])) {
          foreach ($results['file_export'] as $destination_path => $source_path) {
            $path_to_be_removed = str_replace(basename($source_path), '', $source_path);
            $path_to_be_added = 'files/' . str_replace(basename($destination_path), '', $destination_path);
            $archiver->addModify([$source_path], $path_to_be_added, $path_to_be_removed);
          }
        }
        unset($archiver);
      }
      $options = [
        'query' => [
          'token' => \Drupal::service('csrf_token')->get($archive_name),
        ],
      ];
      if ($results['drush'] === TRUE) {
        // Generate own hash to be used as token for controller validation.
        $options['query']['drush'] = 1;
        $token = Crypt::hmacBase64($archive_name, Settings::getHashSalt());
        $state = \Drupal::service('state')->set('migrate_generator_export.token', $token);
        $messenger->addMessage($state);
        $options['query']['token'] = $token;
        $url = Url::fromRoute('migrate_generator_export.export_download', ['uri' => $archive_name], $options)->toString();
        $messenger->addMessage(t('Archive with files can be downloaded @here', ['@here' => $url]));
      }
      else {
        $link = Link::createFromRoute(t('here'), 'migrate_generator_export.export_download', ['uri' => $archive_name], $options)->toString();
        $messenger->addMessage(t('Archive with files can be downloaded @here', ['@here' => $link]));
      }
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage(
        t('An error occurred while processing @operation with arguments : @args'), [
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0]),
        ]
      );
    }
  }

}

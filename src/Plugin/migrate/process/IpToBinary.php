<?php

namespace Drupal\migrate_generator\Plugin\migrate\process;

use Drupal\field_ipaddress\IpAddress;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Convert IP address into binary.
 *
 * @MigrateProcessPlugin(
 *   id = "ip_to_binary",
 *   handle_multiples = false
 * )
 */
class IpToBinary extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (empty($value) || $value === 0) {
      return '';
    }

    try {
      $ip_address = new IpAddress($value);
    }
    catch (\Exception $e) {
      // Skip processing of current value and log message.
      $message = $this->t('Invalid IP or range @value.', ['@value' => $value]);
      $migrate_executable->saveMessage($message);
      return;
    }

    return [
      'ip_start' => inet_pton($ip_address->start()),
      'ip_end' => inet_pton($ip_address->end()),
    ];
  }

}

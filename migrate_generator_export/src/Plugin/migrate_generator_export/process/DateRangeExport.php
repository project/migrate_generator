<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginDateBase;

/**
 * Generator process plugin for "Date range" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "daterange"
 * )
 */
class DateRangeExport extends GeneratorExportPluginDateBase {
}

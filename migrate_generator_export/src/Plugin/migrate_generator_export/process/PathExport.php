<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginBase;

/**
 * Generator export plugin for "Path" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "path"
 * )
 */
class PathExport extends GeneratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity, $field_name, $field_sub_value_name) {
    if (\Drupal::hasService('path_alias.manager')) {
      return \Drupal::service('path_alias.manager')->getAliasByPath('/' . $entity->getEntityTypeId() . '/' . $entity->id());
    }
    return '';
  }

}

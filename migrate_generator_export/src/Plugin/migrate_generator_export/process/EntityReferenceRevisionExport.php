<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

/**
 * Generator process plugin for "Entity reference revisions" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "entity_reference_revisions"
 * )
 */
class EntityReferenceRevisionExport extends EntityReferenceExport {
}

<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generator process plugin for "File" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "file"
 * )
 */
class FileExport extends GeneratorExportPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity, $field_name, $field_sub_value_name) {
    if ($entity->get($field_name)->isEmpty()) {
      return '';
    }
    $file_format = $this->options['file_format'] ?? 'url';
    $field_values = $entity->get($field_name)->getValue();
    $value = [];
    $files = [];
    $file_storage = $this->entityTypeManager->getStorage('file');
    foreach ($field_values as $field_value) {
      if (!empty($field_value['target_id'])) {
        /** @var \Drupal\file\FileInterface $file */
        $file = $file_storage->load($field_value['target_id']);
        if (!empty($file)) {
          $file_uri = $file->getFileUri();
          $realpath = $this->fileSystem->realpath($file_uri);
          $destination = str_replace(':/', '', $file_uri);
          switch ($file_format) {
            case 'filepath_absolute':
              if ($this->options['file_export'] && $this->options['folder']) {
                // If export is made to specific folder - update filepath.
                $files[$destination] = $realpath;
                $value[] = $this->options['folder'] . '/files/' . $destination;
              }
              else {
                $value[] = $realpath;
              }
              break;

            case 'filepath_relative':
              if ($this->options['file_export']) {
                $files[$destination] = $realpath;
                $value[] = 'files/' . $destination;
              }
              break;

            default:
              $value[] = $file->createFileUrl(FALSE);
          }
        }
      }
    }

    $output = implode($this->options['values_delimiter'], $value);

    if (!empty($files)) {
      return [
        'output' => $output,
        'files' => $files,
      ];
    }

    return $output;
  }

}

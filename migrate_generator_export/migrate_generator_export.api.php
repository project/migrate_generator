<?php

/**
 * @file
 * Hooks provided by the Migrate generator export module.
 */

/**
 * Define the list of advanced fields of entity bundle.
 *
 * @param string $entity_type
 *   Entity type.
 * @param string $bundle
 *   Bundle name.
 */
function hook_migrate_generator_export_fields($entity_type, $bundle) {
  $advanced_field_names = [];
  if ($entity_type == 'node' && $bundle == 'page') {
    $advanced_field_names = [
      'vid',
      'revision_timestamp',
      'revision_uid',
      'revision_log',
      'revision_default',
      'revision_translation_affected',
      'content_translation_source',
      'content_translation_outdated',
    ];
  }
  return $advanced_field_names;
}

/**
 * Alter the list of advanced fields of entity bundle.
 *
 * @param string $entity_type
 *   Entity type.
 * @param string $bundle
 *   Bundle name.
 * @param array $advanced_field_names
 *   Advanced fields names array.
 */
function hook_migrate_generator_export_fields_alter($entity_type, $bundle, array &$advanced_field_names) {
  // Remove 'vid' from list of advanced fields for taxonomy terms.
  if ($entity_type == 'taxonomy_term') {
    if (in_array('vid', $advanced_field_names)) {
      unset($advanced_field_names['vid']);
    }
  }
}

<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginDateBase;

/**
 * Generator process plugin for "Date" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "datetime"
 * )
 */
class DateTimeExport extends GeneratorExportPluginDateBase {
}

# migrate_generator_export
Module to generate source CSV files to be processed by migrate_generator

## Basic idea

Main idea here is to **provide funcionality to export content into csv files**
which can be used with `migrate_generator` module.

------------
## Administration

1. Create neccessary configuration export entities on
    `/admin/config/content/migrate-generator-export` page.

    For each entity type and bundle we can have configuration,
    which will be used for export.

    In this configuration you can select fields,
    which will be included in export.

2. Perform export on `/admin/content/migrate-generator-export` page.

    On this page you can see next settings:
    * Checkboxes to select which configured types should be exported.
    * `Include all referenced content` checkbox. With this field checked,
      module will export also content, related via entity reference fields,
      if any of them are included in export.
    * `Delimiter` - Delimiter for source CSV files. Defaults to `;`.
    * `Enclosure` - Enclosure for source CSV files. Defaults to `"`.
    * `Delimiter for multi-valued fields` - Defaults to `|`.
    * `Date/time format` - Date format used in CSV. Defaults to `d-m-Y H:i:s`.
    * `File format` - Format of file attachments in CSV.
      Defaults to `Absolute URL`.
    * `Export folder`. You can fill it to make export into specified folder
      instead of downloading export archive.
    * `Add files themselves to the export` checkbox. With this option checked,
      file attachments will be exported in "files" folder inside archive or
      folder, specified in above option. (Can be used only with
      `Relative file path` and `Absolute file path` file formats)

    Also there is `Save current export settings as default` checkbox to save
    default settings.

## Drush commands

1. Made export of configured entity type
   `migrate_generator_export:export %entity_type_id %entity_bundle %entity_id `

  Where %entity_type_id - Entity type ID, e.g. node, %entity_bundle - bundle,
  e.g. article, %entity_id - optionally provided ID of content to be exported.

    Also there are next possible option:
    * `delimiter` - Delimiter for source CSV files.
    * `enclosure` - Enclosure for source CSV files.
    * `values_delimiter` - Delimiter for multi-valued fields.
    * `date_format` - Date format used in CSV.
    * `file_format` - File format.
                      Can be `url`, `filepath_absolute` or `filepath_relative`.
    * `folder` - Path to folder where exported CSV file should be saved.
    * `file_export` - Save file attachments along with export files.

  This command generates CSV file with name
  `{entity_type_id}-{entity_bundle}.csv`.

2. Made export of configured entity type with all related entity types.
   `migrate_generator_export:export_related %entity_type_id %entity_bundle`

  Where %entity_type_id - Entity type ID, e.g. node, %entity_bundle - bundle,
  e.g. article.

    Also there are next possible option:
    * `delimiter` - Delimiter for source CSV files.
    * `enclosure` - Enclosure for source CSV files.
    * `values_delimiter` - Delimiter for multi-valued fields.
    * `date_format` - Date format used in CSV.
    * `file_format` - File format.
                      Can be `url`, `filepath_absolute` or `filepath_relative`.
    * `folder` - Path to folder where exported CSV file should be saved.
    * `file_export` - Save file attachments along with export files.

  This command generates CSV file with name
  `{entity_type_id}-{entity_bundle}.csv` and CSV files for all related types.

3. Create export configuration for given entity type and bundle.
`migrate_generator_export:create_configs %entity_type_id %entity_bundle`

  Where %entity_type_id - Entity type ID, e.g. node, %entity_bundle - bundle,
  e.g. article.

  Also there is next possible option:
    * fields - comma-separated list of fields to enable for this export
      configuration.

<?php

namespace Drupal\migrate_generator\Plugin;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;

/**
 * The base class for date migrate generator process plugins.
 */
abstract class GeneratorProcessPluginDateBase extends GeneratorProcessPluginBase {

  /**
   * Get destination date format.
   *
   * @return string
   *   Date format.
   */
  protected function getDateFormat() {
    $destination_format = 'U';
    // Get target date format from field storage settings.
    $datetime_type = $this->getFieldStorageDefinition()->getSetting('datetime_type');
    if ($datetime_type) {
      if ($datetime_type == DateRangeItem::DATETIME_TYPE_DATETIME) {
        $destination_format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
      }
      else {
        $destination_format = DateTimeItemInterface::DATE_STORAGE_FORMAT;
      }
    }
    return $destination_format;
  }

  /**
   * Flag to use current date as default value.
   *
   * @return bool
   *   Whether or no to use default current date.
   */
  protected function useDefaultCurrentDate() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseProcess($field_name, $source = NULL, $property = NULL) {
    $process = parent::getBaseProcess($field_name, $source, $property);

    $source_date_format = $this->options['date_format'] ?? 'd-m-Y H:i:s';

    // Use current date as default value if needed.
    if ($this->useDefaultCurrentDate()) {
      // Remove 'skip_on_empty' process.
      foreach ($process as $key => $process_item) {
        if ($process_item['plugin'] == 'skip_on_empty') {
          unset($process[$key]);
        }
      }
      $process[] = [
        'plugin' => 'default_value',
        'default_value' => date($source_date_format),
      ];
    }

    // Check if given property is date field.
    if ($property) {
      $property_definition = $this->getFieldStorageDefinition()->getPropertyDefinition($property);
      if ($property_definition && $property_definition->getDataType() != 'datetime_iso8601') {
        return $process;
      }
    }

    // Get destination date format.
    $destination_format = $this->getDateFormat();

    // Transform date string to proper format.
    $process[] = [
      'plugin' => 'format_date',
      'from_format' => $source_date_format,
      'to_format' => $destination_format,
    ];

    return $process;
  }

}

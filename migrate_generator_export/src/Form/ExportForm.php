<?php

namespace Drupal\migrate_generator_export\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element\Checkboxes;
use Drupal\migrate_generator_export\ExportManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Export form.
 */
class ExportForm extends FormBase implements ContainerInjectionInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity Field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfoService;

  /**
   * Entity export manager service.
   *
   * @var \Drupal\migrate_generator_export\ExportManager
   */
  private $exportManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a ExportForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   Entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service
   *   The bundle info service.
   * @param \Drupal\migrate_generator_export\ExportManager $export_manager
   *   Entity export manager service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityFieldManagerInterface $field_manager, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info_service, ExportManager $export_manager, FileSystemInterface $file_system) {
    $this->configFactory = $config_factory;
    $this->fieldManager = $field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfoService = $bundle_info_service;
    $this->exportManager = $export_manager;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('migrate_generator_export.export_manager'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_generator_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;
    $config_exports = $this->entityTypeManager->getStorage('entity_type_export_config')->loadMultiple();
    if (empty($config_exports)) {
      $form['text'] = [
        '#markup' => $this->t('There is no exports configured. Please, create them on @link page.', ['@link' => Link::createFromRoute($this->t('this'), 'entity.entity_type_export_config.collection')->toString()]),
      ];
      return $form;
    }
    $options = [];
    foreach ($config_exports as $config_export) {
      if (empty($config_export->getFields())) {
        continue;
      }
      $options[$config_export->id()] = $config_export->getName();
    }
    if (empty($options)) {
      $form['text'] = [
        '#markup' => $this->t('There is no exports properly configured. Please, create/update them on @link page.', ['@link' => Link::createFromRoute($this->t('this'), 'entity.entity_type_export_config.collection')->toString()]),
      ];
      return $form;
    }

    $form['exports'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Please, choose exports'),
      '#options' => $options,
    ];

    $form['include_related_content'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include all referenced content.'),
      '#description' => $this->t('Check this option to export content via entity reference fields.'),
    ];

    $config = $this->configFactory->get('migrate_generator_export.settings');
    $form['export_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Export settings'),
      '#collapsible' => TRUE,
      '#tree' => TRUE,
    ];
    $form['export_settings']['delimiter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delimiter'),
      '#description' => $this->t('Add your delimiter (e.g., comma, semi-colon)'),
      '#maxlength' => 2,
      '#size' => 4,
      '#default_value' => $config->get('delimiter'),
    ];

    $form['export_settings']['enclosure'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enclosure'),
      '#description' => $this->t('Enclosure for source CSV files'),
      '#maxlength' => 2,
      '#size' => 4,
      '#default_value' => $config->get('enclosure'),
    ];
    $form['export_settings']['values_delimiter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delimiter for multi-valued fields'),
      '#description' => $this->t('Delimiter for multi-valued fields.'),
      '#maxlength' => 2,
      '#size' => 4,
      '#default_value' => $config->get('values_delimiter'),
    ];
    $form['export_settings']['date_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date/time format'),
      '#description' => $this->t('See <a href="https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters" target="_blank">the documentation for PHP date formats</a>.'),
      '#default_value' => $config->get('date_format'),
    ];
    $form['export_settings']['file_format'] = [
      '#type' => 'radios',
      '#title' => $this->t('File format'),
      '#options' => [
        'url' => $this
          ->t('Absolute URL'),
        'filepath_absolute' => $this
          ->t('Absolute file path'),
        'filepath_relative' => $this
          ->t('Relative file path'),
      ],
      '#default_value' => $config->get('file_format'),
      '#description' => $this->t('Use last option together with "Add files themselves to the export" option on export form to export files along with entities.'),
    ];

    $form['export_settings']['folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Export files into this folder.'),
      '#description' => $this->t('Provide full path to the folder or leave empty to get downloadable archive.'),
      '#default_value' => $config->get('folder'),
    ];

    $form['export_settings']['file_export'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export attachment files'),
      '#description' => $this->t('With this option checked, if exported entities have file fields, corresponding files will be placed in "files" folder inside archive or folder, specified in above option.'),
      '#default_value' => $config->get('file_export'),
      '#states' => [
        'visible' => [
          [
            ':input[name="export_settings[folder]"]' => ['empty' => FALSE],
            ':input[name="export_settings[file_format]"]' => ['value' => 'filepath_absolute'],
          ],
          'or',
          [
            ':input[name="export_settings[file_format]"]' => ['value' => 'filepath_relative'],
          ],
        ],
        'unchecked' => [
          [
            ':input[name="export_settings[folder]"]' => ['filled' => FALSE],
            ':input[name="export_settings[file_format]"]' => ['value' => 'filepath_absolute'],
          ],
          'or',
          [
            ':input[name="export_settings[file_format]"]' => ['value' => 'url'],
          ],
        ],
      ],
    ];

    $form['save'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save current export settings as default'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();
    if ($values['include_related_content']) {
      $errors = [];
      $dependencies = [];
      $entity_types = $this->entityTypeManager->getDefinitions();
      foreach (Checkboxes::getCheckedCheckboxes($values['exports']) as $export_id) {
        // Check referenced dependencies.
        $this->exportManager->checkDependencies($export_id, $entity_types, $dependencies, $errors);
      }
      if ($errors) {
        foreach ($errors as $referenced_bundle => $error) {
          $form_state->setErrorByName($referenced_bundle, $error);
        }
      }
      if ($dependencies) {
        // Add dependent export configs.
        foreach ($dependencies as $dependency) {
          $values['exports'][$dependency] = $dependency;
        }
        $form_state->setValues($values);
      }
    }
    if (!empty($values['export_settings']['folder']) && !$this->fileSystem->prepareDirectory($values['export_settings']['folder'], FileSystemInterface::CREATE_DIRECTORY)) {
      $form_state->setErrorByName('export_settings[folder]', $this->t('There was a problem preparing pointed folder.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $exports = $form_state->getValue('exports');
    if (empty($exports)) {
      return;
    }

    // Save current export settings.
    $export_settings = $form_state->getValue('export_settings');
    if ($form_state->getValue('save')) {
      $config = $this->configFactory->getEditable('migrate_generator_export.settings');
      foreach ($export_settings as $key => $value) {
        $config->set($key, $value);
      }
      $config->save();
    }

    $batch = [
      'title' => $this->t('Export content'),
      'operations' => [],
      'finished' => 'Drupal\migrate_generator_export\ExportManager::batchFinished',
    ];
    $storage = $this->entityTypeManager->getStorage('entity_type_export_config');
    $export_settings['drush'] = FALSE;
    foreach (Checkboxes::getCheckedCheckboxes($exports) as $export_id) {
      $export = $storage->load($export_id);
      if (!empty($export)) {
        $fields = $export->getFields();
        [$entity_type_id, $bundle_id] = explode('--', $export_id);
        $this->exportManager->getOperations($batch['operations'], $entity_type_id, $bundle_id, $fields, $export_settings, NULL);
      }
    }
    if (empty($batch['operations'])) {
      $this->messenger()->addMessage($this->t('No content to export.'));
      return;
    }

    batch_set($batch);
  }

}

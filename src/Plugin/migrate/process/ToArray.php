<?php

namespace Drupal\migrate_generator\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Transform the source value to array.
 *
 * Available configuration keys:
 * - force: (optional) Boolean, if TRUE, force transform to array.
 *   Defaults to FALSE.
 *
 * @MigrateProcessPlugin(
 *   id = "to_array"
 * )
 */
class ToArray extends ProcessPluginBase {

  /**
   * ToArray constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $configuration += [
      'force' => FALSE,
    ];
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $processed_values = $value;
    if (is_string($value) || $this->configuration['force']) {
      $processed_values = [$value];
    }
    return $processed_values;
  }

}

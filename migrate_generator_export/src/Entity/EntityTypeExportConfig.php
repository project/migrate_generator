<?php

namespace Drupal\migrate_generator_export\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Entity Type Export Config entity.
 *
 * @ConfigEntityType(
 *   id = "entity_type_export_config",
 *   label = @Translation("Migrate generator export entity type config"),
 *   label_collection = @Translation("Migrate generator export entity type configs"),
 *   label_singular = @Translation("Migrate generator export entity type config"),
 *   label_plural = @Translation("Migrate generator export entity type configs"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Migrate generator export entity type configs",
 *     plural = "@count Migrate generator export entity type config",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\migrate_generator_export\EntityTypeExportConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\migrate_generator_export\Form\EntityTypeExportConfigForm",
 *       "edit" = "Drupal\migrate_generator_export\Form\EntityTypeExportConfigForm",
 *       "delete" = "Drupal\migrate_generator_export\Form\EntityTypeExportConfigDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     },
 *   },
 *   config_prefix = "entity_type_export_config",
 *   admin_permission = "access csv export",
 *   static_cache = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "fields",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/content/migrate-generator-export/add/types/{entity_type_id}/{bundle}",
 *     "edit-form" = "/admin/config/content/migrate-generator-export/types/{entity_type_id}/{bundle}/{entity_type_export_config}/edit",
 *     "delete-form" = "/admin/config/content/migrate-generator-export/types/{entity_type_export_config}/delete",
 *     "collection" = "/admin/config/content/migrate-generator-export"
 *   }
 * )
 */
class EntityTypeExportConfig extends ConfigEntityBase {

  /**
   * The Entity Type Export Config ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Entity Type Export Config Name.
   *
   * @var string
   */
  protected $name;

  /**
   * List of fields to export.
   *
   * @var array
   */
  protected $fields = [];

  /**
   * Get config name.
   *
   * @return string
   *   Entity Type Export Config Name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Get list of fields to export.
   *
   * @return array
   *   List of fields to export.
   */
  public function getFields() {
    return $this->fields;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    $id = explode('--', $this->id);
    $typeManager = $this->entityTypeManager();
    $dependency = $typeManager->getDefinition($id[0])->getBundleConfigDependency($id[1]);
    $this->addDependency($dependency['type'], $dependency['name']);
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    // The add-form route depends on entity_type_id and bundle.
    if (in_array($rel, ['add-form'])) {
      $parameters = explode('--', $this->id);
      $uri_route_parameters['entity_type_id'] = $parameters[0];
      $uri_route_parameters['bundle'] = $parameters[1];
    }
    return $uri_route_parameters;
  }

}

<?php

namespace Drupal\migrate_generator_export;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Migrate generator export Config entities.
 */
class EntityTypeExportConfigListBuilder extends ConfigEntityListBuilder {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected $entityTypeManager;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfoService;

  /**
   * Constructs new EntityTypeExportConfigListBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The storage.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface|null $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service
   *   The bundle info service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EntityTypeManagerInterface $entityTypeManager, EntityTypeBundleInfoInterface $bundle_info_service) {
    parent::__construct($entity_type, $storage);
    $this->entityTypeManager = $entityTypeManager;
    $this->bundleInfoService = $bundle_info_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'name' => $this->t('Name'),
      'status' => $this->t('Status'),
      'operations' => $this->t('Operations'),
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $list = [
      'table' => [
        '#type' => 'table',
        '#header' => [
          'name' => $this->t('Name'),
          'operations' => $this->t('Operations'),
        ],
      ],
    ];

    $entity_types = $this->entityTypeManager->getDefinitions();
    $storage = $this->entityTypeManager->getStorage('entity_type_export_config');
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if ($entity_type->getGroup() != 'content') {
        continue;
      }
      // Exclude file entity, because it needs special handling.
      if ($entity_type_id == 'file') {
        continue;
      }
      $bundles = $this->bundleInfoService->getBundleInfo($entity_type_id);
      if (empty($bundles)) {
        continue;
      }
      foreach ($bundles as $bundle_id => $bundle) {
        $config_entity = $storage->load($entity_type_id . '--' . $bundle_id);
        if (!empty($config_entity)) {
          $link = [
            'title' => $this->t('Update'),
            'weight' => -10,
            'url' => Url::fromRoute('entity.entity_type_export_config.edit_form', [
              'entity_type_export_config' => $config_entity->id(),
              'entity_type_id' => $entity_type_id,
              'bundle' => $bundle_id,
            ]),
          ];
        }
        else {
          $link = [
            'title' => $this->t('Create'),
            'weight' => -10,
            'url' => Url::fromRoute('entity.entity_type_export_config.add_form', [
              'entity_type_id' => $entity_type_id,
              'bundle' => $bundle_id,
            ]),
          ];
        }
        $row = [
          'name' => ['#markup' => !empty($config_entity) ? '<b>' . $config_entity->getName() . '</b>' : $entity_type->getLabel() . ' - ' . $bundle['label']],
          'operations' => [
            '#type' => 'operations',
            '#links' => [
              'link_create_update' => $link,
            ],
          ],
        ];
        if (!empty($config_entity)) {
          $row['operations']['#links']['delete_link'] = [
            'title' => $this->t('Delete'),
            'weight' => 0,
            'url' => Url::fromRoute('entity.entity_type_export_config.delete_form', [
              'entity_type_export_config' => $config_entity->id(),
            ]),
          ];
        }
        $list['table'][] = $row;
      }
    }

    return $list;
  }

}

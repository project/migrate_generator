<?php

namespace Drupal\migrate_generator_export\Plugin;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base class for all migrate generator export plugins.
 *
 * @see \Drupal\migrate_generator_export\GeneratorExportPluginManager
 * @see \Drupal\migrate_generator_export\Plugin\GeneratorExportPluginInterface
 * @see \Drupal\migrate_generator_export\Annotation\GeneratorExportPluginManager
 * @see plugin_api
 *
 * @ingroup migration
 */
abstract class GeneratorExportPluginBase extends PluginBase implements GeneratorExportPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Generator options.
   *
   * @var array
   */
  protected $options;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->options = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity, $field_name, $field_sub_value_name) {
    if ($entity->get($field_name)->isEmpty()) {
      return '';
    }
    $field_values = $entity->get($field_name)->getValue();
    $value = [];
    foreach ($field_values as $field_value) {
      if (!empty($field_sub_value_name) && !empty($field_value[$field_sub_value_name])) {
        $value[] = $field_value[$field_sub_value_name];
      }
      elseif (!empty($field_value['value'])) {
        $value[] = $field_value['value'];
      }
      elseif (!empty($field_value['target_id'])) {
        $value[] = $field_value['target_id'];
      }
    }

    return implode($this->options['values_delimiter'], $value);
  }

}

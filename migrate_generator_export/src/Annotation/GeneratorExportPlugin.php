<?php

namespace Drupal\migrate_generator_export\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a migrate generator export plugin annotation object.
 *
 * Plugin Namespace: Plugin\migrate_generator_export\process.
 *
 * @see \Drupal\migrate_generator_export\Plugin\GeneratorExportPluginBase
 * @see \Drupal\migrate_generator_export\GeneratorExportPluginManager
 * @see plugin_api
 *
 * @ingroup migration
 *
 * @Annotation
 */
class GeneratorExportPlugin extends Plugin {

  /**
   * The plugin ID = Type of processed field.
   *
   * @var string
   */
  public $id;

}

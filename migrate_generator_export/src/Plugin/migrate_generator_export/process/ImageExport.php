<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

/**
 * Generator process plugin for "Image" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "image"
 * )
 */
class ImageExport extends FileExport {
}

<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginDateBase;

/**
 * Generator export plugin for "Last changed" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "changed"
 * )
 */
class ChangedExport extends GeneratorExportPluginDateBase {
}

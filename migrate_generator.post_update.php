<?php

/**
 * @file
 * Post update functions for Migrate Generator.
 */

/**
 * Rebuild the container to add a new container parameter.
 */
function migrate_generator_post_update_new_container_parameter() {
  // Empty update to cause a cache rebuild so that the container is rebuilt.
}

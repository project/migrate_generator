<?php

namespace Drupal\migrate_generator\Plugin\migrate_generator\process;

use Drupal\migrate_generator\Plugin\GeneratorProcessPluginBase;

/**
 * Generator process plugin for "IP Address" field type.
 *
 * @GeneratorProcessPlugin(
 *   id = "ipaddress"
 * )
 */
class IpAddressGenerator extends GeneratorProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getBaseProcess($field_name, $source = NULL, $property = NULL) {
    $process = parent::getBaseProcess($field_name, $source, $property);
    $process[] = [
      'plugin' => 'ip_to_binary',
    ];

    return $process;
  }

}

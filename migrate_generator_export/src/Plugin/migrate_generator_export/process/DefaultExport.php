<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginBase;

/**
 * Default fallback Generator process plugin.
 *
 * @GeneratorExportPlugin(
 *   id = "default"
 * )
 */
class DefaultExport extends GeneratorExportPluginBase {
}

<?php

namespace Drupal\migrate_generator;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class for migrate generator.
 */
class Generator {

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The generator process plugin manager.
   *
   * @var \Drupal\migrate_generator\GeneratorProcessPluginManager
   */
  protected $processPluginManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The migration plugin manager service.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;

  /**
   * Constructor for Migrate Generator.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\migrate_generator\GeneratorProcessPluginManager $process_plugin_manager
   *   The generator process plugin manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration_plugin_manager
   *   The migration plugin manager.
   */
  public function __construct(LoggerInterface $logger, GeneratorProcessPluginManager $process_plugin_manager, FileSystemInterface $file_system, EntityTypeManagerInterface $entity_type_manager, MigrationPluginManagerInterface $migration_plugin_manager) {
    $this->logger = $logger;
    $this->processPluginManager = $process_plugin_manager;
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
    $this->migrationPluginManager = $migration_plugin_manager;
  }

  /**
   * Remove all migration configs with a given tag.
   *
   * @param string $tag
   *   Migration tag.
   */
  public function deleteMigrationsByTag(string $tag) {
    $migration_storage = $this->entityTypeManager->getStorage('migration');
    $migrations = $this->migrationPluginManager->createInstancesByTag($tag);
    if (!empty($migrations)) {
      foreach ($migrations as $migration) {
        if ($config = $migration_storage->load($migration->id())) {
          $config->delete();
        }
      }
    }
  }

  /**
   * Create migration group for generated migrations if not exists.
   *
   * @param string $entity_type
   *   Destination entity type.
   * @param string $bundle
   *   Destination bundle.
   * @param array $source
   *   Source info array.
   * @param array $options
   *   Additional options.
   * @param bool $is_translation
   *   TRUE for translation migration.
   *
   * @return \Drupal\migrate_plus\Entity\MigrationInterface|null
   *   Migration object or NULL.
   */
  public function createMigration(string $entity_type, string $bundle, array $source, array $options, bool $is_translation = FALSE) {
    $migration_storage = $this->entityTypeManager->getStorage('migration');
    $migration_id = $source['migration_id'];
    if ($is_translation) {
      $migration_id = $migration_id . '__translation';
    }
    // Check if migration exists.
    $migration = $migration_storage->load($migration_id);
    if ($migration) {
      if ($options['update']) {
        // Remove existing migration.
        $migration->delete();
        unset($migration);
      }
      else {
        $this->logger->notice(
          'Migration %migration already exists. Skipping.',
          ['%migration' => $migration_id]
        );
      }
    }
    if (empty($migration)) {
      $label = Unicode::ucfirst($entity_type) . ':' . Unicode::ucfirst($bundle);
      if ($is_translation) {
        $label = $label . ' translation';
      }
      $migrate_properties = [
        'id' => $migration_id,
        'label' => $label,
        'status' => TRUE,
        'migration_tags' => [$options['tag']],
        'source' => $this->setSource($source, $options, $is_translation),
        'destination' => $this->setDestination($entity_type, $bundle, $is_translation),
      ];
      $migration_dependencies = [];
      $migration_process = [];
      // Add some default configuration for translations.
      if ($is_translation) {
        // Add dependency to base migration.
        $migration_dependencies[] = $source['migration_id'];
        if ($source['id_key']) {
          // Add default process to find entity from base migration.
          $migration_process['pseudo_entity_id'] = [
            [
              'plugin' => 'get',
              'source' => $source['id'],
            ],
            [
              'plugin' => 'migration_lookup',
              'migration' => $source['migration_id'],
            ],
            [
              'plugin' => 'skip_on_empty',
              'method' => 'row',
            ],
          ];
          $migration_process[$source['id_key']] = [
            'plugin' => 'get',
            'source' => '@pseudo_entity_id',
          ];
          // We need to fill revision field to paragraphs too.
          if ($entity_type == 'paragraph' && $source['revision_key']) {
            $migration_process[$source['id_key']] = [
              [
                'plugin' => 'get',
                'source' => '@pseudo_entity_id',
              ],
              [
                'plugin' => 'extract',
                'index' => ['0'],
              ],
            ];
            $migration_process[$source['revision_key']] = [
              [
                'plugin' => 'get',
                'source' => '@pseudo_entity_id',
              ],
              [
                'plugin' => 'extract',
                'index' => ['1'],
              ],
            ];
          }
        }
      }
      foreach ($source['fields'] as $field_name => $field_info) {
        $field_info['options'] = $options;
        try {
          $plugin = $this->processPluginManager->createInstance($field_info['field_type'], $field_info);
          // Prepare file destination folder.
          if (in_array($field_info['field_type'], ['file', 'image'])) {
            // Use destination schema storage field settings.
            $directory = $field_info['field_storage_definition']->getSetting('uri_scheme') . '://' . $field_name . DIRECTORY_SEPARATOR;
            $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
          }
          // Get migration process for field.
          $migration_process = array_merge($migration_process, $plugin->process($field_name));
          // Check if we have dependencies for this field.
          if ($field_info['is_reference'] && !empty($field_info['dependencies'])) {
            foreach ($field_info['dependencies'] as $dependency) {
              if (!in_array($dependency, $migration_dependencies)) {
                $migration_dependencies[] = $dependency;
              }
            }
          }
        }
        catch (PluginNotFoundException $ex) {
          // Save log that plugin not found for this field type.
          $this->logger->alert(
            'GeneratorProcessPlugin not found for %field_type field type.',
            ['%field_type' => $field_info['field_type']]
          );
        }
      }
      // Set migration Process.
      $migrate_properties['process'] = $migration_process;
      // Set migration Dependencies.
      $migrate_properties['migration_dependencies'] = [
        'required' => $migration_dependencies,
      ];
      $migration = $migration_storage->create($migrate_properties);
      $migration->save();
      return $migration;
    }
    return NULL;
  }

  /**
   * Set source definition for migration.
   *
   * @param array $source
   *   Source array.
   * @param array $options
   *   Source CSV options.
   * @param bool $is_translation
   *   TRUE for translation migration.
   *
   * @return array
   *   Source definition.
   */
  protected function setSource(array $source, array $options, bool $is_translation = FALSE) {
    $fields = [];
    foreach ($source['header'] as $column) {
      $fields[] = [
        'name' => $column,
      ];
    }
    $ids = [$source['id']];
    if ($is_translation) {
      // Use pair of ID+Langcode as unique id for translation.
      $ids = array_slice($source['header'], 0, 2);
    }
    return [
      'plugin' => 'csv',
      'path' => $source['source'],
      'delimiter' => $options['delimiter'],
      'enclosure' => $options['enclosure'],
      'header_row_count' => 1,
      'fields' => $fields,
      'ids' => $ids,
      'constants' => [],
    ];
  }

  /**
   * Set desination definition for migration.
   *
   * @param string $entity_type
   *   Destination entity type.
   * @param string $bundle
   *   Destination bundle.
   * @param bool $is_translation
   *   TRUE for translation migration.
   *
   * @return array
   *   Destination definition.
   */
  protected function setDestination(string $entity_type, string $bundle, bool $is_translation = FALSE) {
    $destination = [
      'plugin' => 'entity:' . $entity_type,
    ];
    if ($entity_type == 'paragraph') {
      $destination['plugin'] = 'entity_reference_revisions:' . $entity_type;
    }
    if ($entity_type != $bundle) {
      $destination['default_bundle'] = $bundle;
    }
    if ($is_translation) {
      $destination['translations'] = TRUE;
    }
    return $destination;
  }

}

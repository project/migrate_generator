<?php

namespace Drupal\migrate_generator\Commands;

use Drupal\migrate_generator\Generator;
use Drupal\migrate_generator\Scanner;
use Drush\Commands\DrushCommands as DrushCommandsBase;

/**
 * Defines Drush commands for the migrate_generator module.
 */
class DrushCommands extends DrushCommandsBase {

  /**
   * Migrate source scanner.
   *
   * @var \Drupal\migrate_generator\Scanner
   */
  protected $scanner;

  /**
   * Migrate source generator.
   *
   * @var \Drupal\migrate_generator\Generator
   */
  protected $generator;

  /**
   * Constructor method.
   *
   * @param \Drupal\migrate_generator\Scanner $scanner
   *   Class for migrate source scanner.
   * @param \Drupal\migrate_generator\Generator $generator
   *   Class for migrate source generator.
   */
  public function __construct(Scanner $scanner, Generator $generator) {
    parent::__construct();
    $this->scanner = $scanner;
    $this->generator = $generator;
  }

  /**
   * Generate migrations based on source CSVs.
   *
   * @param string $directory
   *   Path to directory with source .csv files.
   * @param array $options
   *   Additional options for the command.
   *
   * @option pattern
   *   Pattern for source CSV files. Defaults to '*.csv'.
   * @option delimiter
   *   Delimiter for source CSV files. Defaults to ;
   * @option enclosure
   *   Enclosure for source CSV files. Defaults to "
   * @option values_delimiter
   *   Delimiter for multi-valued fields. Defaults to |
   * @option date_format
   *   Date format used in CSV. Defaults to "d-m-Y H:i:s"
   * @option relative_filepath
   *   Flag to set if file attachments have relative paths in CSV.
   * @option update
   *   Update previously-generated migrations.
   * @option tag
   *   Migration tag to mark created migrations.
   *
   * @command migrate_generator:generate_migrations
   *
   * @usage drush migrate_generator:generate_migrations $(pwd)/web/modules/custom/migrate_generator/example
   *   Generate migrations from CSV sources from example folder.
   */
  public function generateMigrations($directory, array $options = [
    'pattern' => '*.csv',
    'delimiter' => ';',
    'enclosure' => '"',
    'values_delimiter' => '|',
    'date_format' => 'd-m-Y H:i:s',
    'relative_filepath' => FALSE,
    'update' => FALSE,
    'tag' => 'mgg',
  ]) {
    $options['source_folder'] = $directory;
    // Scan source files.
    $sources = $this->scanner->readSources($directory, $options);
    if ($sources) {
      foreach ($sources as $entity_type => $entity_info) {
        foreach ($entity_info as $bundle => $source_info) {
          $migration = $this->generator->createMigration($entity_type, $bundle, $source_info, $options);
          if ($migration) {
            $this->logger()->notice('Migration ' . $migration->id() . ' was created');
          }
        }
      }
    }
    // Process translations.
    $translation_sources = $this->scanner->readSources($directory, $options, TRUE);
    if ($translation_sources) {
      foreach ($translation_sources as $entity_type => $entity_info) {
        foreach ($entity_info as $bundle => $source_info) {
          $translation_migration = $this->generator->createMigration($entity_type, $bundle, $source_info, $options, TRUE);
          if ($translation_migration) {
            $this->logger()->notice('Translation migration ' . $translation_migration->id() . ' was created');
          }
        }
      }
    }
    $this->logger()->notice(
      'Generation of migrations was completed. You can now run them using next command:
drush migrate:import --all --tag=' . $options['tag']
    );
  }

  /**
   * Delete migrations by tag name.
   *
   * @param string $tags
   *   A comma-separated list of migration tags.
   *
   * @command migrate_generator:clean_migrations
   *
   * @usage migrate_generator:clean_migrations mgg
   *   Delete all migrations with mgg tag.
   */
  public function deleteMigrationsByTags($tags = 'mgg') {
    $tags = array_filter(array_map('trim', explode(',', $tags)));
    foreach ($tags as $tag) {
      $this->generator->deleteMigrationsByTag($tag);
    }
  }

}

<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginDateBase;

/**
 * Generator process plugin for "Date recur" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "date_recur"
 * )
 */
class DateRecurExport extends GeneratorExportPluginDateBase {
}

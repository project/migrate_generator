<?php

namespace Drupal\migrate_generator_export\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\migrate_generator_export\ExportManager;
use Drush\Commands\DrushCommands as DrushCommandsBase;

/**
 * Defines Drush commands for the migrate_generator_export module.
 */
class DrushCommands extends DrushCommandsBase {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfoService;

  /**
   * Entity export manager service.
   *
   * @var \Drupal\migrate_generator_export\ExportManager
   */
  private $exportManager;

  /**
   * The field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $fieldManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Array with export settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * Constructs a new DrushCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service
   *   The bundle info service.
   * @param \Drupal\migrate_generator_export\ExportManager $export_manager
   *   Entity export manager service.
   * @param \Drupal\Core\Entity\EntityFieldManager $field_manager
   *   The entity field manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $bundle_info_service,
    ExportManager $export_manager,
    EntityFieldManager $field_manager,
    FileSystemInterface $file_system,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct();
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfoService = $bundle_info_service;
    $this->exportManager = $export_manager;
    $this->fieldManager = $field_manager;
    $this->fileSystem = $file_system;
    $this->settings = $config_factory->get('migrate_generator_export.settings')->getRawData();
  }

  /**
   * Export entity type.
   *
   * @param string $entity_type_id
   *   Entity type to export.
   * @param string $bundle_id
   *   Entity type bundle to export.
   * @param int $entity_id
   *   Entity to export.
   * @param array $options
   *   Command options.
   *
   * @command migrate_generator_export:export
   *
   * @option delimiter
   *   Delimiter for source CSV files.
   * @option enclosure
   *   Enclosure for source CSV files.
   * @option values_delimiter
   *   Delimiter for multi-valued fields.
   * @option date_format
   *   Date format used in CSV.
   * @option file_format
   *   File format. Can be url, filepath_absolute or filepath_relative.
   * @option folder
   *   Save export files into provided directory.
   * @option file_export
   *   Save files from file fields along with export files.
   *
   * @usage migrate_generator_export:export node basic_page
   *   Export basic_page node type.
   * @usage migrate_generator_export:export node article 1
   *   Export article node with specified ID.
   * @usage migrate_generator_export:export media image --folder=/var/www/html
   *   Export image media type into specified folder.
   * @usage migrate_generator_export:export media image --file-export
   *   Export image media type with files from file fields.
   */
  public function export($entity_type_id, $bundle_id, $entity_id = NULL, array $options = [
    'delimiter' => '',
    'enclosure' => '',
    'values_delimiter' => '',
    'date_format' => '',
    'file_format' => '',
    'folder' => '',
    'file_export' => FALSE,
  ]) {
    $export_settings = $this->getExportSettings($options);
    // Validate export settings.
    $settings_errors = $this->validateExportSettings($export_settings);
    if ($settings_errors) {
      foreach ($settings_errors as $settings_error) {
        $this->messenger()->addError($settings_error);
      }
      return;
    }
    if ($entity_type_id == 'file') {
      $this->messenger()->addStatus($this->t('File entities cannot be exported separately.'));
      return;
    }
    $export_config = $this->entityTypeManager->getStorage('entity_type_export_config')->load($entity_type_id . '--' . $bundle_id);
    if (empty($export_config)) {
      $this->messenger()->addError($this->t(
        'In order to export content, please, create corresponding configuration for @type of type @bundle @here.',
          [
            '@type' => $entity_type_id,
            '@bundle' => $bundle_id,
            '@here' => Link::createFromRoute($this->t('here'), 'entity.entity_type_export_config.add_form',
              [
                'entity_type_id' => $entity_type_id,
                'bundle' => $bundle_id,
              ])->toString(),
          ]
        )
      );
      return;
    }
    $batch = [
      'title' => $this->t('Export content'),
      'operations' => [],
      'finished' => 'Drupal\migrate_generator_export\ExportManager::batchFinished',
    ];
    $fields = $export_config->getFields();
    $export_settings['drush'] = TRUE;
    $this->exportManager->getOperations($batch['operations'], $entity_type_id, $bundle_id, $fields, $export_settings, $entity_id);
    if (empty($batch['operations'])) {
      $this->messenger()->addWarning($this->t('Found no content to export.'));
      return;
    }
    batch_set($batch);
    drush_backend_batch_process();
  }

  /**
   * Export entity type with all related entity types.
   *
   * @param string $entity_type_id
   *   Entity type to export.
   * @param string $bundle_id
   *   Entity type bundle to export.
   * @param array $options
   *   Command options.
   *
   * @command migrate_generator_export:export_related
   * @option delimiter
   *   Delimiter for source CSV files.
   * @option enclosure
   *   Enclosure for source CSV files.
   * @option values_delimiter
   *   Delimiter for multi-valued fields.
   * @option date_format
   *   Date format used in CSV.
   * @option file_format
   *   File format. Can be url, filepath_absolute or filepath_relative.
   * @option folder
   *   Save export files into provided directory.
   * @option file_export
   *   Save files from file fields along with export files.
   *
   * @usage migrate_generator_export:export_related node article
   *   Export article node type with all related types.
   * @usage migrate_generator_export:export_related media image --folder=/var/www/html
   *   Export image media type with related types into specified folder.
   * @usage migrate_generator_export:export_related media image --file-export
   *   Export image media type with related types and files from file fields.
   */
  public function exportRelated($entity_type_id, $bundle_id, array $options = [
    'delimiter' => '',
    'enclosure' => '',
    'values_delimiter' => '',
    'date_format' => '',
    'file_format' => '',
    'folder' => '',
    'file_export' => FALSE,
  ]) {
    $export_settings = $this->getExportSettings($options);
    // Validate export settings.
    $settings_errors = $this->validateExportSettings($export_settings);
    if ($settings_errors) {
      foreach ($settings_errors as $settings_error) {
        $this->messenger()->addError($settings_error);
      }
      return;
    }
    if ($entity_type_id == 'file') {
      $this->messenger()->addStatus($this->t('File entities cannot be exported separately.'));
      return;
    }

    $errors = [];
    $export_configs = [];
    $entity_types = $this->entityTypeManager->getDefinitions();
    $this->exportManager->checkDependencies($entity_type_id . '--' . $bundle_id, $entity_types, $export_configs, $errors);
    if (!empty($errors)) {
      foreach ($errors as $error) {
        $this->messenger()->addError($error);
      }
      return;
    }
    if (empty($export_configs)) {
      $this->messenger()->addError($this->t('Please, check entity type ID and entity bundle to be existing.'));
      return;
    }

    $batch = [
      'title' => $this->t('Export content'),
      'operations' => [],
      'finished' => 'Drupal\migrate_generator_export\ExportManager::batchFinished',
    ];

    $storage = $this->entityTypeManager->getStorage('entity_type_export_config');
    $export_settings['drush'] = TRUE;
    foreach ($export_configs as $export_config_id) {
      $export_config = $storage->load($export_config_id);
      if (empty($export_config)) {
        continue;
      }
      $fields = $export_config->getFields();
      [$entity_type_id, $bundle_id] = explode('--', $export_config_id);
      $this->exportManager->getOperations($batch['operations'], $entity_type_id, $bundle_id, $fields, $export_settings, NULL);
    }

    if (empty($batch['operations'])) {
      $this->messenger()->addWarning($this->t('Found no content to export.'));
      return;
    }

    batch_set($batch);
    drush_backend_batch_process();
  }

  /**
   * Create export configuration entities for given entity type and bundle.
   *
   * @param string $entity_type_id
   *   Entity type export config to be created for.
   * @param string $bundle_id
   *   Entity type bundle export config to be created for.
   * @param array $options
   *   Command options.
   *
   * @command migrate_generator_export:create_configs
   * @option fields Provide comma separated list of field names to be saved.
   *
   * @usage migrate_generator_export:create_configs node basic_page
   *   Create export configuration for basic_page CT with all fields.
   * @usage migrate_generator_export:create_configs node article --fields=title,uid,status
   *   Create export configuration for article CT with specified fields only.
   */
  public function createExportConfigs($entity_type_id, $bundle_id, array $options = ['fields' => NULL]) {
    $entity_types = $this->entityTypeManager->getDefinitions();
    $config_storage = $this->entityTypeManager->getStorage('entity_type_export_config');
    if (empty($entity_types[$entity_type_id])) {
      $this->messenger()->addError($this->t('Please, check entity type ID to be existing.'));
      return;
    }
    $entity_type = $entity_types[$entity_type_id];
    if ($entity_type->getGroup() != 'content') {
      $this->messenger()->addError($this->t('Only content entity types can be exported.'));
      return;
    }
    $bundles = $this->bundleInfoService->getBundleInfo($entity_type_id);
    if (empty($bundles) || (!empty($bundles) && empty($bundles[$bundle_id]))) {
      $this->messenger()->addError($this->t('Please, check entity bundle to be existing.'));
      return;
    }
    $existing = $config_storage->load($entity_type_id . '--' . $bundle_id);
    if (!empty($existing)) {
      $this->messenger()->addStatus($this->t(
      'For @type of type @bundle there is already existing configuration @here.',
        [
          '@here' => Link::createFromRoute($this->t('here'), 'entity.entity_type_export_config.add_form',
            [
              'entity_type_id' => $entity_type_id,
              'bundle' => $bundle_id,
            ])->toString(),
        ]
      ));
      return;
    }
    $entity_type_field_id = $entity_type->getKey('id');
    $fields = [$entity_type_field_id];
    if (!empty($options['fields'])) {
      $fields = array_merge($fields, explode(',', $options['fields']));
    }
    else {
      $fields = $this->getEntityTypeFields($entity_type_id, $bundle_id, $entity_type_field_id);
    }
    $values = [
      'id' => $entity_type_id . '--' . $bundle_id,
      'name' => $entity_type->getLabel() . ' - ' . $bundles[$bundle_id]['label'],
      'fields' => $fields,
    ];
    $export_config = $config_storage->create($values);
    $export_config->save();

    $this->messenger()->addStatus($this->t(
      'You can check created configuration @here.',
       [
         '@here' => Link::createFromRoute($this->t('here'), 'entity.entity_type_export_config.edit_form',
            [
              'entity_type_export_config' => $export_config->id(),
              'entity_type_id' => $entity_type_id,
              'bundle' => $bundle_id,
            ])->toString(),
       ]
    ));
  }

  /**
   * Gets fields for given entity type and bundle.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle_id
   *   Entity bundle ID.
   * @param string $entity_type_field_id
   *   Entity type id field name.
   *
   * @return array
   *   Fields array.
   */
  protected function getEntityTypeFields($entity_type_id, $bundle_id, $entity_type_field_id) {
    $fields = [];
    $entity_bundle_fields = $this->fieldManager->getFieldDefinitions($entity_type_id, $bundle_id);
    $entity_type_storages = $this->fieldManager->getFieldStorageDefinitions($entity_type_id);
    foreach ($entity_bundle_fields as $field_name => $field) {
      if ($entity_type_id == 'user' && $field_name == 'pass') {
        continue;
      }
      if ($field_name == $entity_type_field_id || empty($entity_type_storages[$field_name])) {
        $fields[] = $field_name;
      }
      else {
        $schema = $entity_type_storages[$field_name]->getSchema();
        if (
          empty($schema['columns']) ||
          (!empty($schema['columns']) && count($schema['columns']) == 1) ||
          ($entity_type_storages[$field_name]->getSetting('target_type') == 'file')) {
          $fields[] = $field_name;
        }
        elseif (!empty($schema['columns'])) {
          foreach ($schema['columns'] as $column_name => $column_type) {
            $fields[] = $field_name . '-' . $column_name;
          }
        }
      }
    }

    return $fields;
  }

  /**
   * Get export settings.
   *
   * @param array $options
   *   Overridden export settings.
   *
   * @return array
   *   Export settings array.
   */
  private function getExportSettings(array $options) {
    // Load default settings from config.
    $export_settings = $this->settings;
    // Override default settings.
    $override_options = [
      'delimiter',
      'enclosure',
      'values_delimiter',
      'date_format',
      'file_format',
      'folder',
      'file_export',
    ];
    foreach ($override_options as $option) {
      if ($options[$option]) {
        $export_settings[$option] = $options[$option];
      }
    }
    return $export_settings;
  }

  /**
   * Validate export settings.
   *
   * @param array $export_settings
   *   Export settings array.
   *
   * @return array
   *   Array of errors.
   */
  private function validateExportSettings(array $export_settings) {
    $errors = [];
    // Check file_format option.
    $allowed_formats = ['url', 'filepath_absolute', 'filepath_relative'];
    if ($export_settings['file_format'] &&
      !in_array($export_settings['file_format'], $allowed_formats)) {
      $errors[] = $this->t('Incorrect "file_export" option. Can be "url", "filepath_absolute" or "filepath_relative".');
    }
    // Check if file_export option can be used.
    if (!empty($export_settings['file_export'])) {
      if ($export_settings['file_format'] == 'url') {
        $errors[] = $this->t('Cannot use "file_export" option when "file_format" option is "url".');
      }
      elseif ($export_settings['file_format'] == 'filepath_absolute' && empty($export_settings['folder'])) {
        $errors[] = $this->t('Cannot use "file_export" option when "file_format" option is "filepath_absolute" and "folder" option is empty.');
      }
    }
    // Check export folder.
    if (!empty($export_settings['folder']) && !$this->fileSystem->prepareDirectory($export_settings['folder'], FileSystemInterface::CREATE_DIRECTORY)) {
      $errors[] = $this->t('There was a problem preparing export folder %folder.', ['%folder' => $export_settings['folder']]);
    }
    return $errors;
  }

}

<?php

namespace Drupal\migrate_generator_export\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines an interface for migrate generator export plugin.
 *
 * @see \Drupal\migrate_generator_export\Annotation\GeneratorExportPlugin
 * @see \Drupal\migrate_generator_export\Plugin\GeneratorExportPluginBase
 * @see \Drupal\migrate_generator_export\GeneratorExportPluginManager
 * @see plugin_api
 */
interface GeneratorExportPluginInterface extends PluginInspectionInterface {

  /**
   * Performs the associated export.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to export.
   * @param string $field_name
   *   Field name.
   * @param string $field_sub_value_name
   *   Field column name.
   *
   * @return string|array
   *   Exported value.
   */
  public function process(ContentEntityInterface $entity, $field_name, $field_sub_value_name);

}

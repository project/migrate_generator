<?php

namespace Drupal\migrate_generator_export\Plugin;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;

/**
 * The base class for date migrate generator export plugins.
 */
abstract class GeneratorExportPluginDateBase extends GeneratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity, $field_name, $field_sub_value_name) {
    $field_values = $entity->get($field_name)->getValue();
    $storage_definition = $entity->get($field_name)->getFieldDefinition()->getFieldStorageDefinition();
    $field_type = $storage_definition->getType();
    $value = [];
    $convert_date = TRUE;
    // Check if property is date field.
    if ($field_sub_value_name) {
      $property_definition = $storage_definition->getPropertyDefinition($field_sub_value_name);
      if ($property_definition && $property_definition->getDataType() != 'datetime_iso8601') {
        $convert_date = FALSE;
      }
    }
    $to_format = $this->options['date_format'] ?? 'd-m-Y H:i:s';
    $timestamp_types = ['created', 'changed', 'timestamp'];
    // Get source date format.
    if (in_array($field_type, $timestamp_types)) {
      $from_format = 'U';
    }
    else {
      $datetime_type = $storage_definition->getSetting('datetime_type');
      if ($datetime_type == DateRangeItem::DATETIME_TYPE_DATETIME) {
        $from_format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
      }
      else {
        $from_format = DateTimeItemInterface::DATE_STORAGE_FORMAT;
      }
    }
    foreach ($field_values as $field_value) {
      try {
        $source_value = $field_value['value'];
        if ($field_sub_value_name && isset($field_value[$field_sub_value_name])) {
          $source_value = $field_value[$field_sub_value_name];
        }
        // Convert date format.
        if ($convert_date) {
          $value[] = DateTimePlus::createFromFormat($from_format, $source_value)->format($to_format);
        }
        else {
          $value[] = $source_value;
        }
      }
      catch (\InvalidArgumentException $e) {
        $value[] = '';
      }
      catch (\UnexpectedValueException $e) {
        $value[] = '';
      }
    }

    return implode($this->options['values_delimiter'], $value);
  }

}

<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginBase;

/**
 * Generator export plugin for "Boolean" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "boolean"
 * )
 */
class BooleanExport extends GeneratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function process(ContentEntityInterface $entity, $field_name, $field_sub_value_name) {
    if ($entity->get($field_name)->isEmpty()) {
      return '';
    }
    $field_value = $entity->get($field_name)->getValue();
    return $field_value[0]['value'];
  }

}

<?php

namespace Drupal\migrate_generator_export\Form;

use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base form for entity_type_export_config.
 */
class EntityTypeExportConfigForm extends EntityForm {

  /**
   * The field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $fieldManager;

  /**
   * The current route match.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfoService;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * EntityTypeExportConfigForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManager $field_manager
   *   The entity field manager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP request.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service
   *   The bundle info service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityFieldManager $field_manager, Request $request, TypedConfigManagerInterface $typed_config_manager, EntityTypeBundleInfoInterface $bundle_info_service, ModuleHandlerInterface $module_handler) {
    $this->fieldManager = $field_manager;
    $this->bundleInfoService = $bundle_info_service;
    $this->request = $request;
    $this->typedConfigManager = $typed_config_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('config.typed'),
      $container->get('entity_type.bundle.info'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    // Disable caching on this form.
    $form_state->setCached(FALSE);

    $entity_type_id = $this->request->get('entity_type_id');
    $bundle_id = $this->request->get('bundle');
    if (!$entity_type_id || !$bundle_id) {
      throw new \InvalidArgumentException('Unable to load entity type or bundle for the form.');
    }
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if ($entity_type->getGroup() != 'content') {
      throw new \InvalidArgumentException('Unable to process non-content entity type.');
    }
    /** @var \Drupal\migrate_generator_export\Entity\EntityTypeExportConfig $entity */
    $entity = $this->getEntity();
    $default_values = $entity->getFields();
    $entity_type_field_id = $entity_type->getKey('id');
    $fields = $this->fieldManager->getFieldDefinitions($entity_type_id, $bundle_id);
    $storages = $this->fieldManager->getFieldStorageDefinitions($entity_type_id);
    $bundles = $this->bundleInfoService->getBundleInfo($entity_type_id);
    $config = $this->request->get('entity_type_export_config');
    $create = empty($config) && !empty($entity_type_id) && !empty($bundle_id);
    $form['config_key'] = [
      '#type' => 'hidden',
      '#value' => $entity_type_field_id,
    ];
    $form['id'] = [
      '#type' => 'hidden',
      '#value' => !$create ? $entity->id() : $entity_type_id . '--' . $bundle_id,
    ];
    $form['config_name'] = [
      '#type' => 'hidden',
      '#value' => $entity_type->getLabel() . ' - ' . $bundles[$bundle_id]['label'],
    ];
    $form['#title'] = $this->t('@operation %label export config', [
      '@operation' => $create ? $this->t('Add') : $this->t('Update'),
      '%label' => $form['config_name']['#value'],
    ]);
    if (!empty($fields)) {
      // Get list of advanced fields.
      $advanced_field_names = $this->getAdvancedFields($entity_type_id, $bundle_id);
      $form['main'] = [
        '#type' => 'container',
      ];
      $form['advanced'] = [
        '#type' => 'details',
        '#title' => $this->t('Advanced fields'),
        '#description' => $this->t('These are special fields, which are usually not important for export. These fields may be altered/defined using hook_migrate_generator_export_fields_alter() and hook_migrate_generator_export_fields().'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      ];
      foreach ($fields as $field_name => $field) {
        $group = in_array($field_name, $advanced_field_names) ? 'advanced' : 'main';
        // Remove user password field.
        if ($entity_type_id == 'user' && $field_name == 'pass') {
          continue;
        }
        if ($field_name == $entity_type_field_id) {
          $form[$group]['export_' . $entity_type_field_id] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Export <i>@field</i> field', ['@field' => $field->getName()]),
            '#disabled' => TRUE,
            '#default_value' => TRUE,
          ];
        }
        elseif (empty($storages[$field_name])) {
          $form[$group]['export_' . $field_name] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Export <i>@field</i> field', ['@field' => $field->getName()]),
            '#default_value' => in_array($field_name, $default_values),
          ];
        }
        else {
          $schema = $storages[$field_name]->getSchema();
          if (empty($schema['columns']) || (!empty($schema['columns']) && count($schema['columns']) == 1) || ($storages[$field_name]->getSetting('target_type') == 'file')) {
            $form[$group]['export_' . $field_name] = [
              '#type' => 'checkbox',
              '#title' => $this->t('Export <i>@field</i> field', ['@field' => $field->getName()]),
              '#default_value' => in_array($field_name, $default_values),
            ];
          }
          elseif (!empty($schema['columns'])) {
            foreach ($schema['columns'] as $column_name => $column_type) {
              $form[$group]['export_' . $field_name . '-' . $column_name] = [
                '#type' => 'checkbox',
                '#title' => $this->t(
                  'Export <i>@field</i> field (@column column)', [
                    '@field' => $field->getName(),
                    '@column' => $column_name,
                  ]),
                '#default_value' => in_array($field_name . '-' . $column_name, $default_values),
              ];
            }
          }
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $typed_config = $this->typedConfigManager
      ->createFromNameAndData($this->entity->id(), $this->entity->toArray());
    $constraints = $typed_config->validate();
    /** @var \Symfony\Component\Validator\ConstraintViolation $violation */
    foreach ($constraints as $violation) {
      $form_path = str_replace('.', '][', $violation->getPropertyPath());
      $form_state->setErrorByName($form_path, $violation->getMessage());
    }
  }

  /**
   * Get list of advanced fields.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Bundle name.
   *
   * @return array
   *   List of advanced fields
   */
  protected function getAdvancedFields($entity_type, $bundle) {
    $advanced_field_names = [];
    $implementors = [];
    $hook = 'migrate_generator_export_fields';
    if ($this->moduleHandler->hasImplementations($hook)) {
      $this->moduleHandler->invokeAllWith($hook, function (callable $hook, string $module) use (&$implementors) {
        $implementors[] = $module;
      });
      // Get list of advanced fields.
      foreach ($implementors as $implementor) {
        $module_definitions = $this->moduleHandler->invoke(
          $implementor, 'migrate_generator_export_fields', [
            $entity_type, $bundle,
          ]);
        if (!empty($module_definitions)) {
          foreach ($module_definitions as $field_name) {
            $advanced_field_names[$field_name] = $field_name;
          }
        }
      }
    }

    // Alter advanced fields list.
    $this->moduleHandler->alter('migrate_generator_export_fields', $entity_type, $bundle, $advanced_field_names);

    return $advanced_field_names;
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    parent::copyFormValuesToEntity($entity, $form, $form_state);
    $values = $form_state->getValues();
    // Put ID field by default.
    $fields = [$values['config_key']];
    foreach ($values as $field_name => $value) {
      if (strpos($field_name, 'export_') === 0 && $value == 1) {
        $field_name = mb_substr($field_name, 7);
        if ($field_name != $values['config_key']) {
          $fields[] = $field_name;
        }
      }
    }
    $entity->set('fields', $fields);
    $entity->set('name', $values['config_name']);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $export_config = $this->entity;
    $status = $export_config->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label export.', [
          '%label' => $export_config->getName(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label export.', [
          '%label' => $export_config->getName(),
        ]));
    }
    $form_state->setRedirectUrl($export_config->toUrl('collection'));
  }

}

<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginDateBase;

/**
 * Generator process plugin for "Timestamp" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "timestamp"
 * )
 */
class TimestampExport extends GeneratorExportPluginDateBase {
}

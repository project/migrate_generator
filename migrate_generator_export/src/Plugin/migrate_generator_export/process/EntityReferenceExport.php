<?php

namespace Drupal\migrate_generator_export\Plugin\migrate_generator_export\process;

use Drupal\migrate_generator_export\Plugin\GeneratorExportPluginBase;

/**
 * Generator process plugin for "Entity reference" field type.
 *
 * @GeneratorExportPlugin(
 *   id = "entity_reference"
 * )
 */
class EntityReferenceExport extends GeneratorExportPluginBase {
}
